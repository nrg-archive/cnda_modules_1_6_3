#!/bin/bash

BUILDPATH="."
REPO_URL="https://bitbucket.org"


if [ -s ./module_manifest.txt ]; then
   while read source_repo
   do
       repo=`echo $source_repo | awk -F/ '{print $2}'`
       repoOwn=`echo ${source_repo} | awk -F/ '{print $1}'`
       rm ${repoOwn}-${repo}-*.zip
       dateTime=`date +%Y%m%d_%H%M`
       curl -k ${REPO_URL}/${repoOwn}/${repo}/get/tip.zip > ${BUILDPATH}/${repo}_${dateTime}.zip
       if [ ! -s ${BUILDPATH}/${repo}_${dateTime}.zip ]; then
         echo Cannot find ${BUILDPATH}/${repo}_${dateTime}.zip
         exit 1;
       fi
       cd ${BUILDPATH}
       unzip ${BUILDPATH}/${repo}_${dateTime}.zip
       zipDir=`ls | grep "${repoOwn}-${repo}-"`
       cd ${zipDir}
       zip -r ../${zipDir}.zip * 
       cd ..
       rm -rf ${zipDir}
       rm ${repo}_${dateTime}.zip
   done < ./module_manifest.txt
else
   echo "Could not find module_manifest.txt."
fi

